'use strict';

/**
 * @ngdoc overview
 * @name ng-name-here
 * @description
 * # ng-name-here
 *
 * Main module of the application.
 */
angular

.module('ng-name-here', [
	'ngResource',
	'ui.router',
	'angular-gestures',
	'ngAnimate'
])

.config(['$stateProvider', '$urlRouterProvider', 'hammerDefaultOptsProvider',
	function ($stateProvider, $urlRouterProvider, hammerDefaultOptsProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: 'views/main.html',
				controller: 'MainCtrl'
			});
			hammerDefaultOptsProvider.set({
				// Add recognizers here:
				recognizers: [[Hammer.Pan, {time: 250}]]
			});
	}
])

.run(['$rootScope','$http',
	function ($scope, $http) {
		// App version info
		$http
		.get('version.json')
		.then(function (v) {
			$scope.version = v.version;
			$scope.appName = v.name;
		});
	}
]);
