'use strict';

/**
 * @ngdoc function
 * @name ng-name-here.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ng-name-here
 */
angular.module('ng-name-here')
.controller('MainCtrl', [
	'$scope',
	function ($scope) {
		// CHANGE:
		$scope = $scope;
		// init()

	}
]);
