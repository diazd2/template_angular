## Synopsis

This template provides all you need to start a new AngularJS project. In order to set up the project, use *grunt function*s and edit *Gruntfile.js*. Only app.js and main.js are present. No API's used. Only font-awesome is included in template.
You will need nodejs and npm installed, as well as grunt, grunt-cli and bower

This version requires nodejs >= 0.12.0

## grunt Functions

* grunt deploy:dev
* grunt deploy:prod
* grunt deployopen:dev
* grunt deployopen:prod
* grunt bump:pre
* grunt bump:bug
* grunt bump:minor
* grunt bump:major
* grunt serve
* grunt build
* grunt (by default will run jshint and grunt build)

## Renaming App

Use **grunt renameapp:newNameForApp**
The script will automatically define the name in the places where it must be replaced. ('App' is appended to name entered, where needed)

## Versioning

Initial version is 0.0.0. Use grunt **bump:[type]** to version up.

## Update repository URL

Use **git remote set-url origin [SSH URL to repository]**

## EditorConfig

If using Sublime Text, install EditorConfig through Package Control (see .editorconfig for details)